# ------------------------------------------------------------------------------
# ~/.bashrc.d/legacy_aliases.sh — Santiago Andrés Ardiles Páez
# ------------------------------------------------------------------------------

# --------------------------------------
# Uncategorised
# --------------------------------------
alias aptupgrade='sudo apt update && sudo apt upgrade --assume-yes && sudo apt autoremove'
alias checkottoexport="~/Documents/otto-product-export-checker/otto-export-checker.py"
alias gitallsubmodulestomaster='find . -maxdepth 1 -type d \( ! -name . \) -exec bash -c "cd '{}' && git checkout master" \;'
alias gobereithiervor='find . -type f \( -name "*.xlsm" -o -name "*.xlsx" \) -exec ./go-prepare.py "{}" \;'
alias ottocategoriesjson='vim /home/santiago/Downloads/OTTO/OTTO_Categories/OTTO_Categories.json'
alias pythonblacklinelength100='black --line-length=100'
alias pythonblacklinelength80='black --line-length=80'
alias removeansiescapesequences="sed -r 's/'$(echo -e "\033")'\[[0-9]{1,2}(;([0-9]{1,2})?)?[mK]//g'"
alias xclippy='xclip -sel clip'

# --------------------------------------
# Akeneo PIM
# --------------------------------------
alias akeneodevmoinimporttest='docker compose exec -T fpm \
	./vendor/bin/phpunit -c tests/PHPUnit/phpunit.xml.dist --filter testMoinImportJob \
	/srv/pim/src/Adventos/Bundle/OttoConnectorBundle/Tests/Integration/OttoMoinIntegration.php'
alias akeneodevxlsximporttest='docker compose exec -T fpm \
	./vendor/bin/phpunit -c tests/PHPUnit/phpunit.xml.dist --filter testXlsxImportJob \
	/srv/pim/src/Adventos/Bundle/GojungoConnectorBundle/Tests/Integration/XlsxProductImportIntegration.php'
alias akeneodevcsvimporttest='docker compose exec -T fpm \
	./vendor/bin/phpunit -c tests/PHPUnit/phpunit.xml.dist --filter testCsvImportJob \
	/srv/pim/src/Adventos/Bundle/GojungoConnectorBundle/Tests/Integration/CsvProductImportIntegration.php'
alias akeneodevzalandoproductexporttest='docker compose exec -T fpm \
	./vendor/bin/phpunit -c tests/PHPUnit/phpunit.xml.dist --filter testZalandoProductExportJob \
	/srv/pim/src/Adventos/Bundle/ZalandoConnectorBundle/Tests/Integration/ZalandoProductExportIntegration.php'
alias akeneodevsalesgeneralservicesimporttest='docker compose exec -T fpm \
	./vendor/bin/phpunit -c tests/PHPUnit/phpunit.xml.dist --filter testGeneralServicesImportJob \
	/srv/pim/src/Adventos/Bundle/SalesBundle/Tests/Integration/SalesJobsIntegration.php'
alias akeneodevpimformat='docker compose exec -T fpm \
	vendor/bin/php-cs-fixer fix --diff --config=.php_cs.dist'
alias akeneodevcreateadminuser='docker compose exec fpm \
	bin/console pim:user:create admin admin admin@admin.admin Admin User en_US --admin -n --env=prod'
alias akeneoproddownloadprodlog='scp root@go-pim.adventos.de:/opt/gojungo/pim/var/logs/prod.log  .'
alias gotogojungopim='ssh root@go-pim.adventos.de'
alias htoppy='ssh root@gojungo.com -t htop'
alias tailpimprodlog='ssh root@gojungo.com -t tail -f /opt/gojungo/pim/var/logs/prod.log'
alias tailpimshopifylog='ssh root@gojungo.com -t tail -f /opt/gojungo/pim/var/logs/webkul_shopify_batch.prod.log'
alias dryrsyncpim="rsync --dry-run \
	--checksum \
	--itemize-changes \
	--links \
	--progress \
	--recursive \
	--verbose \
	--group \
	--owner \
	-D \
	~/Git/GoJungo/go/pim/gojungo-pim-6/ root@go-pim.adventos.de:/opt/gojungo/pim6/ \
	--exclude='.git' \
	--exclude='.env*' \
	--exclude='vendor' \
	--exclude='node_modules' \
	--exclude='var' \
	--exclude='web' \
	--exclude='app/file_storage' \
	--exclude='app/archive' \
	--exclude='./public' \
	--exclude='*~' \
	--exclude='config/secrets' \
	--filter='dir-merge,- .gitignore'"
alias wetrsyncpim="rsync \
	--checksum \
	--itemize-changes \
	--links \
	--progress \
	--recursive \
	--verbose \
	--group \
	--owner \
	-D \
	~/Git/GoJungo/go/pim/gojungo-pim-6/ root@go-pim.adventos.de:/opt/gojungo/pim6/ \
	--exclude='.git' \
	--exclude='.env*' \
	--exclude='vendor' \
	--exclude='node_modules' \
	--exclude='var' \
	--exclude='web' \
	--exclude='app/file_storage' \
	--exclude='app/archive' \
	--exclude='./public' \
	--exclude='*~' \
	--exclude='config/secrets' \
	--filter='dir-merge,- .gitignore'"

# --------------------------------------
# GoJungo Portal
# --------------------------------------
alias portalmongorestore='docker compose exec mongodb \
	mongorestore -d gomongo "/home/backups/gomongo$(date '+%Y%m%d')/gomongo"'
alias portalrsyncmongodatabase='rsync -avh \
	root@gojungo.com:/opt/gojungo/portal/goback/mongo_backups/ \
	~/Documents/gojungo-portal/submodules/gojungo-portal-back/mongo_backups/'

# --------------------------------------
# Tracycle
# --------------------------------------
alias gototracy='ssh root@tracemyshirt.com'
alias tracydryrsync="rsync --dry-run \
	--progress \
	--verbose \
	--recursive \
	--itemize-changes \
	--links \
	--perms \
	-D \
	--checksum ~/Git/GoJungo/brands-fashion/tracycle/ root@tracemyshirt.com:/var/www/html/tracycle_v3/ \
	--include='/public' \
	--exclude='.env' \
	--exclude='.git' \
	--exclude='.idea' \
	--exclude='.prettierrc' \
	--exclude='.vscode' \
	--exclude='/node_modules' \
	--exclude='/tools' \
	--exclude='/vendor' \
	--exclude='.gitignore' \
	--exclude='.gitattributes' \
	--exclude='*.log' \
	--filter='- /.gitignore'"
alias tracywetrsync="rsync \
	--progress \
	--verbose \
	--recursive \
	--itemize-changes \
	--links \
	--perms \
	-D \
	--checksum ~/Git/GoJungo/brands-fashion/tracycle/ root@tracemyshirt.com:/var/www/html/tracycle_v3/ \
	--include='/public' \
	--exclude='.env' \
	--exclude='.git' \
	--exclude='.idea' \
	--exclude='.prettierrc' \
	--exclude='.vscode' \
	--exclude='/node_modules' \
	--exclude='/tools' \
	--exclude='/vendor' \
	--exclude='.gitignore' \
	--exclude='.gitattributes' \
	--exclude='*.log' \
	--filter='- /.gitignore'"
alias tracyupdatelocaldb='gototracy -t "mysqldump -p tracycle > Temporal_Tracycle_Backup_$(date '+%Y%m%d').sql -u root -p" && scp root@tracemyshirt.com:/root/Temporal_Tracycle_Backup_$(date '+%Y%m%d').sql $HOME && cat $HOME/Temporal_Tracycle_Backup_$(date '+%Y%m%d').sql | docker exec -i tracemyshirt_mysql_1 mysql -u tracy --password=tracy tracemyshirt'

# --------------------------------------
# Go-to servers
# --------------------------------------
alias gotoshopware="ssh adventos@go-sw6-01.adventos.de"
alias gotomule="ssh root@bfmule04.brands.web-brands.de"
alias gotoportal="ssh root@go-portal.adventos.de"
