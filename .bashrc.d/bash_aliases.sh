# ------------------------------------------------------------------------------
# ~/.bashrc.d/bash_aliases.sh — Santiago Andrés Ardiles Páez
# ------------------------------------------------------------------------------

# --------------------------------------
# ls
# --------------------------------------
alias ll="ls -l \
	--almost-all \
	--color=always \
	--human-readable \
	--file-type"

# --------------------------------------
# xclip
# --------------------------------------
alias xclippy="xclip -selection clipboard"
alias xclippynl="xclip -rmlastnl -selection clipboard"

# --------------------------------------
# unoconv
# --------------------------------------
alias allxlsmtoxlsx='find . -type f \
	-name "*.xlsm" \
	-exec unoconv -f xlsx "{}" \;'
alias allxlsxtocsv='find . -type f \
	-name "*.xlsx" \
	-exec unoconv -f csv -e FilterOptions="59,34,0,1,1/5/2/1/3/1/4/1" "{}" \;'

# --------------------------------------
# git
# --------------------------------------
alias gits="git status"
alias allgitcheckouttomaster='find . -maxdepth 1 \
	-type d \( ! -name . \) \
	-exec bash -c "cd '{}' && git checkout master" \;'

# --------------------------------------
# stow
# --------------------------------------
alias stowdotfiles='stow \
	--restow \
	--verbose=5 \
	--dir="$HOME/Stows/" . \
	--target="$HOME/"'

# --------------------------------------
# Emacs
# --------------------------------------
alias "e"="GTK_THEME=Adwaita:dark emacs"
alias "ec"="GTK_THEME=Adwaita:dark emacsclient"
ethisquarter() {
	GTK_THEME=Adwaita:dark emacs "$HOME/Agenda/$(date +%Y-Q%q).org" \
		--eval='(text-scale-increase 1)' &
}
alias "eopeninit"='GTK_THEME=Adwaita:dark emacs "$HOME/Stows/.config/emacs/init.el" &'
alias "ecopeninit"='GTK_THEME=Adwaita:dark emacsclient -r "$HOME/Stows/.config/emacs/init.el" &'
alias "ecthisquarter"='GTK_THEME=Adwaita:dark emacsclient -r "$HOME/Agenda/$(date +%Y-Q%q).org" &'

# --------------------------------------
# Evolution
# --------------------------------------
alias "darkevolution"="GTK_THEME=Adwaita:dark evolution &"

# --------------------------------------
# Neovim
# --------------------------------------
alias v="nvim"

# --------------------------------------
# ~/.cache/
# --------------------------------------
alias "clear-100-days-old-cache"="find ~/.cache/ -type f -atime +100 -delete"

# --------------------------------------
# Navigation
# --------------------------------------
alias "gotodesktop"='cd "$HOME/Desktop/"'
alias "gotoprojects"='cd "$HOME/Git/Projects/"'
alias "gotostows"='cd "$HOME/Stows/"'
alias "gotoagenda"='cd "$HOME/Agenda/"'
alias "gotodictionary"='cd "$HOME/Dictionary/"'
alias "gotobeaver"="cd ~/.local/share/DBeaverData/"
