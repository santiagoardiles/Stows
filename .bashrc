# ------------------------------------------------------------------------------
# ~/.bashrc — Santiago Andrés Ardiles Páez
#
# This file is meant to complement Fedora’s default bash configuration files;
# which can be found in the following files and directories:
#
# - /etc/bashrc
# - /etc/bash_completion.d/
# - /etc/profile
# - /etc/profile.d/
# ------------------------------------------------------------------------------

# --------------------------------------
# If not running interactively, don’t do anything. (Necessary in Ubuntu.)
# --------------------------------------
[[ $- != *i* ]] && return

# --------------------------------------
# Enable color support of ‘ls’. (Necessary in Ubuntu.)
# --------------------------------------
if [ -x /usr/bin/dircolors ]; then
	test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
	alias ls='ls --color=auto'
	alias dir='dir --color=auto'
	alias vdir='vdir --color=auto'
	alias grep='grep --color=auto'
	alias fgrep='fgrep --color=auto'
	alias egrep='egrep --color=auto'
fi

# --------------------------------------
# Enable programmable completion features (you don't need to enable this, if
# it's already enabled in /etc/bash.bashrc and /etc/profile sources
# /etc/bash.bashrc). (Necessary in Ubuntu.)
# --------------------------------------
if ! shopt -oq posix; then
	if [ -f /usr/share/bash-completion/bash_completion ]; then
		. /usr/share/bash-completion/bash_completion
	elif [ -f /etc/bash_completion ]; then
		. /etc/bash_completion
	fi
fi

# --------------------------------------
# Source global definitions
# --------------------------------------
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# --------------------------------------
# User-specific environment
# --------------------------------------
if ! [[ "$PATH" =~ "$HOME/.local/bin:$HOME/bin:" ]]; then
	PATH="$HOME/.local/bin:$HOME/bin:$PATH"
fi
export PATH

# --------------------------------------
# systemctl’s auto-paging feature
# --------------------------------------
# export SYSTEMD_PAGER=

# --------------------------------------
# User-specific aliases and functions
# --------------------------------------
if [ -d ~/.bashrc.d ]; then
	for rc in ~/.bashrc.d/*; do
		if [ -f "$rc" ]; then
			. "$rc"
		fi
	done
fi
unset rc

# --------------------------------------
# Prompts
# --------------------------------------

# -----------------
# Primary prompt
#
# This one is Red Hat’s default; ‘PS1="[\u@\h \W]\\$ "’.
# -----------------
if [ "$HOSTNAME" = "vmi999125.contaboserver.net" ]; then
	export PS1="\[\033[38;5;14m\][\u@\h \w]\[\033[0m\]\\$ "
elif [ "$HOSTNAME" = "go-dash" ]; then
	export PS1="\[\033[38;5;10m\][\u@\h \w]\[\033[0m\]\\$ "
elif [ "$HOSTNAME" = "ip-92-205-62-220.ip.secureserver.net" ]; then
	export PS1="\[\033[38;5;13m\][\u@\h \w]\[\033[0m\]\\$ "
else
	export PS1="[\w]\\$ "
fi

# --------------------------------------
# History
# --------------------------------------
export HISTCONTROL=ignoreboth:erasedups
export HISTFILESIZE=-1
export HISTSIZE=-1

# --------------------------------------
# Editors
#
# See https://unix.stackexchange.com/a/334022/479809/
# --------------------------------------
export VISUAL=nvim
export EDITOR=nvim

# --------------------------------------
# Software flow control
# --------------------------------------
stty stop '^P'

# --------------------------------------
# Auto-completion
# --------------------------------------
bind 'set show-all-if-ambiguous on'
bind 'TAB:menu-complete'
bind '"\e[Z":menu-complete-backward' # So ⇧ + ⭾ search backwards.

# --------------------------------------
# nvm
#
# Commenting out as, for some reason, I don’t really want to use nvm anymore.
# --------------------------------------
# export NVM_DIR="$HOME/.nvm"
# [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"
# [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"

# --------------------------------------
# GHCup
# --------------------------------------
[ -f "$HOME/.ghcup/env" ] && source "$HOME/.ghcup/env"

# --------------------------------------
# PATH
# --------------------------------------
export PATH="$HOME/.config/composer/vendor/bin:$PATH"

# --------------------------------------
# SDKMAN!
# --------------------------------------
export SDKMAN_DIR="$HOME/.sdkman"
[[ -s "$HOME/.sdkman/bin/sdkman-init.sh" ]] && source "$HOME/.sdkman/bin/sdkman-init.sh"

# ------------------------------------------------------------------------------
# Experiments
# ------------------------------------------------------------------------------

# --------------------------------------
# Necessary as tmux and screen will complain otherwise.
# --------------------------------------
if [ -n "$TMUX" ] || [ -n "$STY" ]; then
	__vte_prompt_command() {
		true
	}
fi

# --------------------------------------
# Time utilities
# --------------------------------------
# countdown() {
#	start="$(( $(date +%s) + $1))"
#	while [ "$start" -ge $(date +%s) ]; do
#		## Is this more than 24h away?
#		days="$(($(($(( $start - $(date +%s) )) * 1 )) / 86400))"
#		time="$(( $start - `date +%s` ))"
#		printf '%s day(s) and %s\r' "$days" "$(date -u -d "@$time" +%H:%M:%S)"
#		sleep 0.1
#	done
# }

# stopwatch() {
#	start=$(date +%s)
#	while true; do
#		now=$(date +%s)
#		days="$(($((now - start)) / 86400))"
#		difference="$((now - start))"
#		formatted=$(date -u -d "@$difference" +'%H:%M:%S')
#		printf '%s day(s) and %s\r' "$days" "$formatted"
#		sleep 0.1
#	done
# }

# --------------------------------------
# All GTK applications open through the terminal will be dark-themed.
# --------------------------------------
# export GTK_THEME=Adwaita:dark

# --------------------------------------
# Saves command upon entering it.
# --------------------------------------
if [ -z "$PROMPT_COMMAND" ]; then
	export PROMPT_COMMAND="history -a"
else
	export PROMPT_COMMAND="${PROMPT_COMMAND}; history -a"
fi
